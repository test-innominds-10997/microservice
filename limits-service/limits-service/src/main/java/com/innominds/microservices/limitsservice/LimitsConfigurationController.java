package com.innominds.microservices.limitsservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LimitsConfigurationController {

	@Autowired
	Configuration configuration;

	@GetMapping("/limits-hardcode")
	public LimitsConfiguration retrieveLimitsFromConfigurationsHC() {
		return new LimitsConfiguration(1000, 10);
	}

	@GetMapping("/limits")
	public LimitsConfiguration retrieveLimitsFromConfigurations() {

		return new LimitsConfiguration(configuration.getMaximum(), configuration.getMinimum());
	}

}
